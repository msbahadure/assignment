import backtrader as bt
import yfinance as yf
from backtesting.lib import crossover


yf.pdr_override()


class Stg(bt.Strategy):
    params = dict(
        size=25,
        stop_loss=1,
        take_profit=2,
        RsiInd=14,
        FSma=10,
        SSma=30,
    )

    def __init__(self):
        self.take_profit_1 = 0
        self.take_profit_2 = 0
        self.loss_1 = 0
        self.loss_2 = 0
        self.startcash = self.broker.getvalue()
        self.rsi = bt.indicators.RSI_SMA(self.data.close, period=self.params.RsiInd)
        self.sma1 = bt.indicators.SMA(self.data.close, period=self.params.FSma)
        self.sma2 = bt.indicators.SMA(self.data.close, period=self.params.SSma)

    def next(self):
        if not self.position:
            if crossover(self.rsi, 30) or crossover(self.sma1, self.sma2):
                self.buy(size=self.p.size)
                self.take_profit_1 = self.data.close[0] * (1.0 + self.p.take_profit / 100)
                self.loss_1 = self.data.close[0] * (1.0 - self.p.stop_loss / 100)

            if self.rsi >= 70:
                self.sell(size=self.p.size)
                self.take_profit_2 = self.data.close[0] * (1.0 - self.p.take_profit / 100)
                self.loss_2 = self.data.close[0] * (1.0 + self.p.stop_loss / 100)
        else:
            if self.position.size > 0:  # if size +ve means self.buy performed
                if self.rsi > 70:
                    self.close(size=self.p.size)

                elif self.take_profit_1 < self.data.close:
                    self.close(size=self.p.size)

                elif self.loss_1 > self.data.close:
                    self.close(size=self.p.size)
            else:
                if self.rsi[0] < 30:
                    self.close(size=self.p.size)

                elif self.take_profit_2 > self.data.close[0]:
                    self.close(size=self.p.size)

                elif self.loss_2 > self.data.close[0]:
                    self.close(size=self.p.size)


startcash = 100000  # float(input("Enter StartCash: "))

cerebro = bt.Cerebro(optreturn =False)
def floats_range(start, stop, step):
    while stop > start:
        yield start
        start += step
cerebro.optstrategy(Stg, stop_loss=floats_range(0.5,2.5,0.5),take_profit=floats_range(0.5,2.5,0.5),RsiInd=range(14,21,7),FSma=range(10,20,5),SSma=range(30,40,10))

data = bt.feeds.PandasData(dataname=yf.download('TATAMOTORS.NS', '2022-06-01', '2022-07-06', interval='15m'))
'''data = bt.feeds.GenericCSVData(
    dataname='WIPRO.NS.csv',
    fromdate=datetime.datetime(2020, 1, 1),
    todate=datetime.datetime(2020, 3, 27),
    dtformat=('%Y-%m-%d'),
)'''
cerebro.adddata(data)
cerebro.broker.setcash(startcash)
cerebro.broker.setcommission(commission=0.002)
opt_runs = cerebro.run()
final_results_list = []
for run in opt_runs:
    for strategy in run:
        value = round(strategy.broker.get_value(),2)
        PnL = round(value - startcash,2)
        profit = strategy.params.take_profit
        Loss = strategy.params.stop_loss
        period = strategy.params.RsiInd
        Fsma = strategy.params.FSma
        Ssma = strategy.params.SSma
        final_results_list.append([profit,Loss,PnL,period,Fsma,Ssma])

by_profit = sorted(final_results_list, key=lambda x: x[0])
by_PnL = sorted(final_results_list, key=lambda x: x[2], reverse=True)
for result in by_profit:
    print('Take Profit: {}, Stop Loss: {}, Rsi: {}, Fast_SMA: {}, Slow_SMA: {}, PnL: {}'.format(result[0], result[1], result[3], result[4], result[5], result[2]))

