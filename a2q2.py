import backtrader as bt
import yfinance as yf
import datetime
from backtesting.lib import crossover

yf.pdr_override()


class Stg(bt.Strategy):
    params = dict(
        size=25,
        stop_loss=0.8,
        take_profit=2.5,
        trail=0.1
    )

    def __init__(self):
        self.take_profit = 0
        self.loss = 0
        self.d1= self.getdatabyname("d1_5m")
        self.d2= self.getdatabyname("d2_15m")

        # self.rsi = bt.indicat,ors.RSI_SMA(self.data.close, period=14)
        # self.sma1 = bt.indicators.SMA(self.data.close, period=10)
        # self.sma2 = bt.indicators.SMA(self.data.close, period=30)

    def next(self):
        if not self.position:
            # print(self.d2.datetime.datetime(0).isoformat())
            # print(self.d1.datetime.datetime(0).isoformat())
            if datetime.time(9, 30) < self.data.datetime.time() < datetime.time(15, 30):
                if self.d1.close[-1] < self.d2.high[0]:
                    self.buy(size=self.p.size)
                    self.take_profit = self.d2.high[0] * (1.0 + self.p.take_profit / 100)
                    self.loss = self.d2.high[0] * (1.0 - self.p.stop_loss / 100)
        else:
            if datetime.time(9, 30) < self.data.datetime.time() < datetime.time(15, 30):
                if self.d1.close[-1] > self.d2.low[0]:
                    self.sell(size=self.p.size)

                elif self.take_profit < self.d2.close[0]:
                    self.sell(size=self.p.size)

                elif self.loss > self.d2.close[0]:
                    if not self.p.trail:
                        self.sell(size=self.p.size)
                    else:
                        self.sell(size=self.p.size, trailpercent=self.p.trail)


startcash = 100000  # float(input("Enter StartCash: "))

cerebro = bt.Cerebro()
cerebro.addstrategy(Stg)

data1 = bt.feeds.PandasData(dataname=yf.download('TATAMOTORS.NS', '2022-06-01', '2022-07-06', interval='5m'))
data2 = bt.feeds.PandasData(dataname=yf.download('TATAMOTORS.NS', '2022-06-01', '2022-07-06', interval='15m'))
'''data = bt.feeds.GenericCSVData(
    dataname='WIPRO.NS.csv',
    fromdate=datetime.datetime(2020, 1, 1),
    todate=datetime.datetime(2020, 3, 27),
    dtformat=('%Y-%m-%d'),
)'''
cerebro.adddata(data1, name="d1_5m")
cerebro.adddata(data2, name="d2_15m")

cerebro.broker.setcash(startcash)
cerebro.broker.setcommission(commission=0.002)
cerebro.run()
portvalue = cerebro.broker.getvalue()
pnl = portvalue - startcash
print('Final Portfolio Value: Rs.{}'.format(portvalue))
print('P/L: Rs.{}'.format(pnl))
cerebro.plot(style='candlestick')
