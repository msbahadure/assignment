import backtrader as bt
import yfinance as yf
import datetime
from backtesting.lib import crossover

yf.pdr_override()


class Stg(bt.Strategy):
    params = dict(
        size=25,
        stop_loss=1,
        take_profit=2,
    )

    def __init__(self):
        self.take_profit = 0
        self.loss = 0
        self.startcash = self.broker.getvalue()
        self.rsi = bt.indicators.RSI_SMA(self.data.close, period=14)
        self.sma1 = bt.indicators.SMA(self.data.close, period=10)
        self.sma2 = bt.indicators.SMA(self.data.close, period=30)

    print("It's for Short Position")

    def next(self):
        if not self.position:
            if self.rsi >= 70:
                self.sell(size=self.p.size)
                self.take_profit = self.data.close[0] * (1.0 - self.p.take_profit / 100)
                self.loss = self.data.close[0] * (1.0 + self.p.stop_loss / 100)
        else:
            if self.rsi[0] < 30:
                self.buy(size=self.p.size)

            elif self.take_profit > self.data.close[0]:
                self.buy(size=self.p.size)

            elif self.loss > self.data.close[0]:
                self.buy(size=self.p.size)


startcash = 100000  # float(input("Enter StartCash: "))  # 100000

cerebro = bt.Cerebro(optreturn=False)


# cerebro.addstrategy(Stg)
def floats_range(start, stop, step):
    while start < stop:
        yield start
        start += step


cerebro.optstrategy(Stg, stop_loss=floats_range(0.5, 5, 0.5), take_profit=floats_range(0.5, 5, 0.5))

data = bt.feeds.PandasData(dataname=yf.download('TATAMOTORS.NS', '2022-06-01', '2022-07-06', interval='15m'))
'''data = bt.feeds.GenericCSVData(
    dataname='WIPRO.NS.csv',
    fromdate=datetime.datetime(2020, 1, 1),
    todate=datetime.datetime(2020, 3, 27),
    dtformat=('%Y-%m-%d'),
)'''
cerebro.adddata(data)
cerebro.broker.setcash(startcash)
cerebro.broker.setcommission(commission=0.002)
# cerebro.run()
# cerebro.plot(style='candlestick')
opt_runs = cerebro.run()
final_results_list = []
for run in opt_runs:
    for strategy in run:
        value = round(strategy.broker.get_value(), 2)
        PnL = round(value - startcash, 2)
        profit = strategy.params.take_profit
        Loss = strategy.params.stop_loss
        final_results_list.append([profit, Loss, PnL])

by_profit = sorted(final_results_list, key=lambda x: x[0])
# by_loss = sorted(final_results_list, key=lambda x: x[1])
by_PnL = sorted(final_results_list, key=lambda x: x[2], reverse=True)

for result in by_profit:
    print('Take Profit: {}, Stop Loss: {}, PnL: {}'.format(result[0], result[1], result[2]))
