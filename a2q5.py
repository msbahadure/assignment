import backtrader as bt
import yfinance as yf
import datetime
from backtesting.lib import crossover

yf.pdr_override()


class SwingInd(bt.Indicator):
    lines = ('swings', 'signal')
    params = (
        ('period', None),
    )

    def __init__(self):
        self.swing_range = (self.p.period * 2) + 1
        self.addminperiod(self.swing_range)

    def next(self):
        highs = self.data.high.get(size=self.swing_range)
        lows = self.data.low.get(size=self.swing_range)
        if highs.pop(self.p.period) > max(highs):
            self.lines.swings[-self.p.period] = 1
            self.lines.signal[0] = 1
        elif lows.pop(self.p.period) < min(lows):
            self.lines.swings[-self.p.period] = -1
            self.lines.signal[0] = -1
        else:
            self.lines.swings[-self.p.period] = 0
            self.lines.signal[0] = 0


class Stg(bt.Strategy):
    params = dict(
        size=10,
        stop_loss=1,
        take_profit=2,
    )

    def __init__(self):
        self.take_profit = 0
        self.loss = 0
        self.ind=SwingInd(self.data, period=14)

    def next(self):
        if not self.position:
            if self.ind.signal[0] < 0:
                #print("p1", self.datas[0].datetime.date())
                self.sell(size=self.p.size)
                self.take_profit = self.data.close[0] * (1.0 - self.p.take_profit / 100)
                self.loss = self.data.close[0] * (1.0 + self.p.stop_loss / 100)
        else:
            #print(self.position.datetime.date())
            if self.position.datetime.date() == self.datas[0].datetime.date():
                if self.ind.signal[0] == 1:
                    self.buy(size=100)

                elif self.take_profit > self.data.close[0]:
                    self.buy(size=self.p.size)

                elif self.loss > self.data.close[0]:
                    self.buy(size=self.p.size)


startcash = 100000  # float(input("Enter StartCash: "))

cerebro = bt.Cerebro()
cerebro.addstrategy(Stg)

data = bt.feeds.PandasData(dataname=yf.download('TATAMOTORS.NS', '2022-06-01', '2022-07-06', interval='15m'))
'''data = bt.feeds.GenericCSVData(
    dataname='WIPRO.NS.csv',
    fromdate=datetime.datetime(2020, 1, 1),
    todate=datetime.datetime(2020, 3, 27),
    dtformat=('%Y-%m-%d'),
)'''
cerebro.adddata(data)
#Converting data to heikinashi data
data1=data.clone()
data1.addfilter(bt.filters.HeikinAshi)
cerebro.adddata(data1)
cerebro.broker.setcash(startcash)
cerebro.broker.setcommission(commission=0.002)
cerebro.run()
portvalue = cerebro.broker.getvalue()
pnl = portvalue - startcash
print('Final Portfolio Value: Rs.{}'.format(portvalue))
print('P/L: Rs.{}'.format(pnl))
cerebro.plot(style='heikin ashi')
