import backtrader as bt
import yfinance as yf
import datetime
from backtesting.lib import crossover

yf.pdr_override()


class Stg(bt.Strategy):
    params = dict(
        size=25,
        stop_loss=0.8,
        take_profit=2.5,
        trail=0.1
    )

    def __init__(self):
        self.take_profit = 0
        self.loss = 0
        self.rsi = bt.indicators.RSI_SMA(self.data.close, period=14)
        self.sma1 = bt.indicators.SMA(self.data.close, period=10)
        self.sma2 = bt.indicators.SMA(self.data.close, period=30)

    def next(self):
        if not self.position:
            if crossover(self.rsi, 30) or crossover(self.sma1,self.sma2):
                self.buy(size=self.p.size)
                self.take_profit = self.data.close[0] * (1.0 + self.p.take_profit / 100)
                self.loss = self.data.close[0] * (1.0 - self.p.stop_loss / 100)
        else:
            if self.rsi > 70:
                self.sell(size=self.p.size)

            # elif bt.ind.CrossUp(bt.ind.RSI(),70):
            elif self.take_profit < self.data.close[0]:
                self.sell(size=self.p.size)

            elif self.loss > self.data.close[0]:
                if not self.p.trail:
                    self.sell(size=self.p.size)
                else:
                    self.sell(size=self.p.size, trailpercent=self.p.trail)


startcash = 100000  # float(input("Enter StartCash: "))  # 100000

cerebro = bt.Cerebro()
cerebro.addstrategy(Stg)

data = bt.feeds.PandasData(dataname=yf.download('TATAMOTORS.NS', '2022-06-01', '2022-07-06', interval='15m'))
'''data = bt.feeds.GenericCSVData(
    dataname='WIPRO.NS.csv',
    fromdate=datetime.datetime(2020, 1, 1),
    todate=datetime.datetime(2020, 3, 27),
    dtformat=('%Y-%m-%d'),
)'''
cerebro.adddata(data)
cerebro.broker.setcash(startcash)
cerebro.broker.setcommission(commission=0.002)
cerebro.run()
portvalue = cerebro.broker.getvalue()
pnl = portvalue - startcash
print('Final Portfolio Value: Rs.{}'.format(portvalue))
print('P/L: Rs.{}'.format(pnl))
cerebro.plot(style='candlestick')
